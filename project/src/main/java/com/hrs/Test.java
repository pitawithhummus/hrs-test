package com.hrs;

import com.hrs.controllers.Readings;
import com.hrs.controllers.Results;
import com.hrs.controllers.Rules;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class Test {
    Logger log = LogManager.getFormatterLogger();

    public static void main(String[] args) throws InterruptedException {
        String readingsFile = args.length >= 1 ? args[0] : null;
        String rulesFile = args.length >= 2 ? args[1] : "rules.json";
        new Test(rulesFile, readingsFile);
        Thread.sleep(5);
    }

    public Test(String rulesFile, String readingsFile) {
        log.info("Starting application");
        try {
            // Read resource source file for rules
            Rules rules = new Rules();
            rules.load(rulesFile);

            // Read resource source file for values
            Readings readings = new Readings();
            readings.load(readingsFile);

            // See which values trigger the rules
            Results results = new Results();
            results.process(readings, rules);

            // Report the findings to another server
            //     (don't need to make the actual http request but code should lead up until that final moment)
            log.info("Syncing...");
            results.post("URL_GOES_HERE"); // handles 500 and other error in functin
            log.info("Data sync success");

            // Write findings as json file to /code/results.json
            log.info("Writing to results.json...");
            results.toFile();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        log.info("Done");
    }
}
