package com.hrs.models;

import java.util.HashMap;
import java.util.Map;

public enum Operator {
    GREATER_THAN(">"),
    LESS_THAN("<"),
    GREATER_EQUAL(">="),
    LESS_EQUAL("<=");
    String value;

    private static final Map<String, Operator> valueMap = new HashMap<>();

    static {
        for (Operator status : Operator.values()) {
            valueMap.put(status.getValue(), status);
            valueMap.put(status.toString(), status);
        }
    }

    Operator(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Operator from(String value) {
        return valueMap.get(value);
    }
}
