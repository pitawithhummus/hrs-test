package com.hrs.models;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RuleDeserializer extends StdDeserializer<Rule<Reading>> {

    public RuleDeserializer() {
        this(null);
    }

    public RuleDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Rule<Reading> deserialize(JsonParser jp, DeserializationContext ctxt) throws
            IOException {
        Rule<Reading> rule = new Rule<>();
        JsonNode node = jp.getCodec().readTree(jp);
        Map<ReadingSubType, RuleLine> rules = new HashMap<>();
        node.fieldNames().forEachRemaining(fn -> {
            ArrayNode arrayNode = (ArrayNode) node.get(fn);
            rules.put(ReadingSubType.from(fn),
                    new RuleLine(Operator.from(arrayNode.get(0).asText()),
                            arrayNode.get(1).asInt()));
        });
        rule.setRules(rules);
        return rule;
    }
}
