package com.hrs.models;

import java.util.Map;

public class Result<T extends IReading> {
    private Map<ReadingSubType, String> findings;
    private ReadingType type;

    public Result() {
    }

    public ReadingType getType() {
        return type;
    }

    public void setType(ReadingType type) {
        this.type = type;
    }

    public Map<ReadingSubType, String> getFindings() {
        return findings;
    }

    public void setFindings(Map<ReadingSubType, String> findings) {
        this.findings = findings;
    }

}
