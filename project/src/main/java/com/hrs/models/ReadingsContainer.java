package com.hrs.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReadingsContainer {
    private int id;
    private List<Reading> readings;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Reading> getReadings() {
        return readings;
    }

    public void setReadings(List<Reading> readings) {
        this.readings = readings;
    }

    public ResultsContainer<Reading> asses(Map<ReadingType, Rule<Reading>> rules) {
        return new ResultsContainer<>()
                .setId(id)
                .setResults(readings.stream().map(reading ->
                        reading.asses(rules.get(reading.getType())))
                        .collect(Collectors.toList()));
    }

    @Override
    public String toString() {
        return "ReadingsContainer{" +
                "id=" + id +
                ", readings=" + readings +
                '}';
    }
}
