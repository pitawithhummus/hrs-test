package com.hrs.models;

import java.util.HashMap;
import java.util.Map;

public enum ReadingType {
    BLOOD_PRESSURE("bloodPressure"),
    GLUCOSE("glucose"),
    WEIGHT("weight");
    String value;
    private static final Map<String, ReadingType> valueMap = new HashMap<>();

    static {
        for (ReadingType status : ReadingType.values()) {
            valueMap.put(status.getValue(), status);
            valueMap.put(status.toString(), status);
        }
    }

    ReadingType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static ReadingType from(String value) {
        return valueMap.get(value);
    }
}
