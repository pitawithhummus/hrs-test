package com.hrs.models;

public interface IRule<T extends IReading> {
    Result<T> apply(T reading);
}
