package com.hrs.models;

import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.HashMap;
import java.util.Map;

@JsonRootName("glucose")
public class Glucose extends Reading {
    private int bloodSugarLevel;

    public int getBloodSugarLevel() {
        return bloodSugarLevel;
    }

    public void setBloodSugarLevel(int bloodSugarLevel) {
        this.bloodSugarLevel = bloodSugarLevel;
    }

    @Override
    public <T extends Reading> Result<T> asses(Rule<T> rule) {
        Result<T> result = new Result<>();
        result.setType(getType());
        Map<ReadingSubType, String> results = new HashMap<>();
        results.put(ReadingSubType.BLOOD_SUGAR,
                rule.getRules().get(ReadingSubType.BLOOD_SUGAR).isInRange(bloodSugarLevel) ?
                        "OK" : "Exceeds Range");
        result.setFindings(results);
        return result;    }
}
