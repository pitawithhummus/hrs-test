package com.hrs.models;

import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.HashMap;
import java.util.Map;

@JsonRootName("bloodPressure")
public class BloodPressure extends Reading {
    private int systolic, diastolic, heartRate;

    public BloodPressure() {
    }

    public int getSystolic() {
        return systolic;
    }

    public void setSystolic(int systolic) {
        this.systolic = systolic;
    }

    public int getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(int diastolic) {
        this.diastolic = diastolic;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    @Override
    public <T extends Reading> Result<T> asses(Rule<T> rule) {
        Result<T> result = new Result<>();
        result.setType(getType());
        Map<ReadingSubType, String> results = new HashMap<>();
        results.put(ReadingSubType.SYSTOLIC,
                rule.getRules().get(ReadingSubType.SYSTOLIC).isInRange(systolic) ?
                        "OK" : "Exceeds Range");
        results.put(ReadingSubType.DIASTOLIC,
                rule.getRules().get(ReadingSubType.DIASTOLIC).isInRange(diastolic) ?
                        "OK" : "Exceeds Range");
        results.put(ReadingSubType.HEART_RATE,
                rule.getRules().get(ReadingSubType.HEART_RATE).isInRange(heartRate) ?
                        "OK" : "Exceeds Range");
        result.setFindings(results);
        return result;
    }
}
