package com.hrs.models;

public interface IReading {
    <T extends Reading> Result<T> asses(Rule<T> rule);
}
