package com.hrs.models;

public class RuleLine {

    private Operator operator;
    private Integer value;

    public RuleLine() {
    }

    public RuleLine(Operator operator, Integer value) {
        this.operator = operator;
        this.value = value;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public boolean isInRange(int val) {
        switch (operator) {
            case LESS_THAN:
                return value < val;
            case LESS_EQUAL:
                return value <= val;
            case GREATER_THAN:
                return value > val;
            case GREATER_EQUAL:
                return value >= val;
            default:
                throw new RuntimeException(String.format("Unimplemented Operator: %s", operator));
        }
    }

}

