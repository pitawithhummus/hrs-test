package com.hrs.models;

import com.fasterxml.jackson.annotation.JsonRootName;

import java.util.HashMap;
import java.util.Map;

@JsonRootName("weight")
public class Weight extends Reading {
    private int weight;

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public <T extends Reading> Result<T> asses(Rule<T> rule) {
        Result<T> result = new Result<>();
        result.setType(getType());
        Map<ReadingSubType, String> results = new HashMap<>();
        results.put(ReadingSubType.WEIGHT,
                rule.getRules().get(ReadingSubType.WEIGHT).isInRange(weight) ?
                        "OK" : "Exceeds Range");
        result.setFindings(results);
        return result;
    }

    @Override
    public String toString() {
        return "Weight{" +
                "weight=" + weight +
                '}';
    }
}
