package com.hrs.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultsContainer<T extends Reading> {
    private int id;
    private List<Result<T>> results;

    public int getId() {
        return id;
    }

    public ResultsContainer<T> setId(int id) {
        this.id = id;
        return this;
    }

    public List<Result<T>> getResults() {
        return results;
    }

    public ResultsContainer<T> setResults(List<Result<T>> results) {
        this.results = results;
        return this;
    }
}
