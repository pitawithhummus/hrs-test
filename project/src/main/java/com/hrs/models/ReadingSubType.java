package com.hrs.models;

import java.util.HashMap;
import java.util.Map;

public enum ReadingSubType {
    SYSTOLIC("systolic"),
    DIASTOLIC("diastolic"),
    HEART_RATE("heartRate"),
    GLUCOSE("glucose"),
    BLOOD_SUGAR("bloodSugarLevel"),
    WEIGHT("weight");
    String value;
    private static final Map<String, ReadingSubType> valueMap = new HashMap<>();

    static {
        for (ReadingSubType status : ReadingSubType.values()) {
            valueMap.put(status.getValue(), status);
            valueMap.put(status.toString(), status);
        }
    }

    ReadingSubType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static ReadingSubType from(String value) {
        return valueMap.get(value);
    }

    @Override
    public String toString() {
        return value;
    }
}
