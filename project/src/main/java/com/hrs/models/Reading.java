package com.hrs.models;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "type",
        visible = true)
@JsonSubTypes({@JsonSubTypes.Type(value = BloodPressure.class, name = "bloodPressure"),
        @JsonSubTypes.Type(value = Glucose.class, name = "glucose"),
        @JsonSubTypes.Type(value = Weight.class, name = "weight")})
public abstract class Reading implements IReading {
    private ReadingType type;

    public Reading() {
    }

    public ReadingType getType() {
        return type;
    }

    public void setType(ReadingType type) {
        this.type = type;
    }

}

