package com.hrs.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Map;

@JsonDeserialize(using = RuleDeserializer.class)
public class Rule<T extends Reading> implements IRule<T> {
    Map<ReadingSubType, RuleLine> rules;

    public Map<ReadingSubType, RuleLine> getRules() {
        return rules;
    }

    public void setRules(Map<ReadingSubType, RuleLine> rules) {
        this.rules = rules;
    }

    @Override
    public Result<T> apply(T reading) {
        return reading.asses(this);
    }

}
