package com.hrs.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hrs.models.Reading;
import com.hrs.models.ReadingType;
import com.hrs.models.Rule;

import java.io.IOException;
import java.util.Map;

public class Rules extends Controller<Map<ReadingType, Rule<Reading>>> {
    private Map<ReadingType, Rule<Reading>> rules;

    public Map<ReadingType, Rule<Reading>> getRules() {
        return rules;
    }

    public void setRules(Map<ReadingType, Rule<Reading>> rules) {
        this.rules = rules;
    }

    @Override
    public Map<ReadingType, Rule<Reading>> load(String fileName) throws IOException {
        Map<ReadingType, Rule<Reading>> ruleMap = Mapper.getInstance().getMapper()
                .readValue(fileAsString(fileName),
                new TypeReference<Map<ReadingType, Rule<Reading>>>() {});
        setRules(ruleMap);
        return ruleMap;
    }
}
