package com.hrs.controllers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public final class Mapper {
    private final YAMLFactory yamlFactory;
    private final ObjectMapper mapper;
    private static Mapper instance = null;

    private Mapper() {
        yamlFactory = new YAMLFactory();
        mapper = new ObjectMapper(yamlFactory);
        mapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
        mapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
    }

    public static Mapper getInstance() {
        if (instance == null) instance = new Mapper();
        return instance;
    }

    public YAMLFactory getYamlFactory() {
        return yamlFactory;
    }

    public ObjectMapper getMapper() {
        return mapper;
    }
}
