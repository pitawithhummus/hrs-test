package com.hrs.controllers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hrs.models.Reading;
import com.hrs.models.ResultsContainer;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class Results {
    private final HttpClient httpClient = new HttpClient();
    private List<ResultsContainer<Reading>> results;

    public List<ResultsContainer<Reading>> getResults() {
        return results;
    }

    public void setResults(List<ResultsContainer<Reading>> results) {
        this.results = results;
    }

    public void process(Readings readings, Rules rules) {
        setResults(readings.getReadings().stream().map(readingsContainer ->
                readingsContainer.asses(rules.getRules())).collect(Collectors.toList()));
    }

    public ResponseBody post(String url) throws IOException {
      return httpClient.sendPost(url, RequestBody.create(Mapper.getInstance().getMapper().writeValueAsBytes(results)));
    }

    public void toFile() throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(new File("results.json"));
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
        mapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
        fileOutputStream.write(mapper.writeValueAsString(results).getBytes());
    }
}
