package com.hrs.controllers;

import java.io.IOException;
import java.io.InputStream;

public abstract class Controller<T> implements FileLoader<T> {

    protected String fileAsString(String fileName) throws IOException {
        InputStream stream = getClass().getClassLoader().getResourceAsStream(fileName);
        if (stream == null) throw new RuntimeException("Error loading file " + fileName);
        byte[] buffer = new byte[stream.available()];
        stream.read(buffer);
        return new String(buffer);
    }
}
