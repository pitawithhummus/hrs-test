package com.hrs.controllers;

import java.io.IOException;

public interface FileLoader<T> {
    T load(String fileName) throws IOException;
}
