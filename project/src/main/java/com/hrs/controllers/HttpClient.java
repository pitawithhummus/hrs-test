package com.hrs.controllers;

import okhttp3.*;

import java.io.IOException;

public class HttpClient {

    private final OkHttpClient httpClient = new OkHttpClient();

    public ResponseBody sendPost(String url, RequestBody body) throws IOException {
        if (!url.startsWith("http")) return null;
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Content-Type", "application/json")
                .post(body)
                .build();
        Response response = httpClient.newCall(request).execute();
        if (!response.isSuccessful()) {
            if (response.code() == 500) throw new RuntimeException("Internal server error");
            throw new RuntimeException("Error: " + response.message());
        }
        return response.body();
    }

}
