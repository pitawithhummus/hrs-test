package com.hrs.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;
import com.fasterxml.jackson.dataformat.yaml.YAMLParser;
import com.hrs.models.Reading;
import com.hrs.models.ReadingsContainer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Readings extends Controller<List<ReadingsContainer>> {
    private List<ReadingsContainer> readings;

    public List<ReadingsContainer> getReadings() {
        return readings;
    }

    public void setReadings(List<ReadingsContainer> readings) {
        this.readings = readings;
    }

    @Override
    public List<ReadingsContainer> load(String fileName) throws IOException {
        final String extension = fileName.split("\\.")[fileName.split("\\.").length - 1];
        ObjectMapper mapper = Mapper.getInstance().getMapper();
        String fileStr = fileAsString(fileName);
        final List<ReadingsContainer> list;
        switch (extension) {
            case "json":
                // Sanitize json string (Test sample file contains '\n')
                fileStr = fileAsString(fileName).replaceAll("\\\\n", "");
                list = mapper.readValue(fileStr,
                        new TypeReference<List<ReadingsContainer>>() {
                        });
                break;

            case "yml":
                final YAMLParser parser = Mapper.getInstance().getYamlFactory().createParser(fileStr);
                final JsonNode node = mapper.readTree(parser);
                list = new ArrayList<>();
                node.fieldNames().forEachRemaining(id -> {
                    List<Reading> readings = new ArrayList<>();
                    ReadingsContainer readingsContainer = new ReadingsContainer();
                    readingsContainer.setId(Integer.parseInt(id));
                    node.get(id).get("readings").fieldNames().forEachRemaining(readingType -> {
                        JsonNode jsonNode = node.get(id).get("readings").get(readingType);
                        ObjectNode k = ((ObjectNode) jsonNode).put("type", readingType);
                        TreeTraversingParser treeTraversingParser = new TreeTraversingParser(k);
                        try {
                            readings.add(mapper.readValue(treeTraversingParser, Reading.class));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        readingsContainer.setReadings(readings);
                    });
                    list.add(readingsContainer);
                });
                break;
            default:
                // Implement other file types here
                throw new RuntimeException("Unimplemented file type: " + extension);
        }
        setReadings(list);
        return list;
    }
}
